const express = require('express');
const fs = require('fs');
const bodyParser = require('body-parser');
const cmd = require('node-cmd');
const app = express();
const generateCustomSet = require('./utils/customFont')
const CustomizedIconsPack = require('./utils/customImage')
const port = process.env.PORT || 3131;
const svgToPng = require('./utils/svgToPng')
const { MongoClient } = require("mongodb");
const { postAnalyticsData } = require('./utils/analytics')
const dotEnv = require('dotenv')

// use env variables
app.set(dotEnv.config())

const mongoClient = new MongoClient(process.env.MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true });

async function connectDatabase() {
  try {

    await mongoClient.connect();

  } finally {

    console.log("Connected successfully to server");

  }
}


// connect to database for production env only
if (process.env.NODE_ENV == "production")
  connectDatabase().catch(console.dir);

app.use(bodyParser.json());
app.use(express.static(`${__dirname}/dist`));

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get("/", function (req, res) {
  res.send('✅ Server running. <br><br> Run eos-icons-landing to test it. <br>Repo https://gitlab.com/SUSE-UIUX/eos-icons-landing')
});


app.get('/icon/svg/download/:iconName', function (req, res) {
  const iconName = req.params.iconName
  const file = `${__dirname}/svg/${iconName}.svg`
  const requestHost = req.get('host');
  postAnalyticsData(requestHost, mongoClient, {icons: [iconName]}, "svg", false)
  res.download(file);
});

app.get('/icon/png/download/:iconName/:pngSize', function (req, res) {
  const iconName = req.params.iconName
  const pngSize = req.params.pngSize
  const requestHost = req.get('host');
  postAnalyticsData(requestHost, mongoClient, {icons: [iconName]}, "png", false )

  svgToPng(iconName, pngSize)
    .then(() => {
      const file = `${__dirname}/temp/${iconName}_${pngSize}.png`
      res.download(file);
    }).catch(err => {
      res.send(`Couldn't generate PNG for ${iconName}`)
    })
});

app.post("/icon-customization", function (req, res) {
  timestamp = Math.floor(Date.now())
  customSet = new CustomizedIconsPack(req.body, timestamp)
  customSet.generatePack()
  const requestHost = req.get('host');
  postAnalyticsData(requestHost, mongoClient, req.body, req.body.exportAs, true)
  res.send(`${timestamp}`)
});


//download path for get request
app.get('/download', function (req, res) {
  var ts = req.query.ts;
  var dist = `dist_${ts}`;
  var file = `${__dirname}/temp/${dist}.zip`
  res.download(file);
});

//download path to download single icon
app.get('/icon-download', function (req, res) {
  var ts = req.query.ts
  var type = req.query.type
  var iconName = req.query.iconName
  var file = `${__dirname}/temp/dist_${ts}/${type}/${iconName}.${type}`
  res.download(file);
});

app.post("/iconsapi", function (req, res) {
  //Icon names through api req
  icons = req.body.icons;

  //Timestamp at time of user clicks
  timestamp = Math.floor(Date.now());

  //creating instance of class
  customFontSet = new generateCustomSet(icons, timestamp);

  //generating final command
  gruntCommand = customFontSet.commandFinal();
  console.log(gruntCommand);

  cmd.get(
    gruntCommand,
    function (err, data, stderr) {
      customFontSet.generateFiles();
      res.send(`${timestamp}`);
    }
  );
  const requestHost = req.get('host');
  postAnalyticsData(requestHost, mongoClient, req.body, "font", false)
});


app.listen(port, function () {
  console.log("🤖 Server started \n👉 localhost:3131");
});
